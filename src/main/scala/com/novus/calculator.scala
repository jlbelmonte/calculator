package com.novus.calculator

import scala.annotation.tailrec
import scala.util.control.NonFatal
import scala.util.{Failure, Success, Try}

trait CalcTerm
trait HigherPrecedence extends Operand

abstract class Operand extends CalcTerm {
  def op(t1: Double, t2: Double): Double
}

object Operand {
  case object plus extends Operand { override def op(x: Double, y: Double) = x + y }
  case object minus extends Operand with CalcTerm { override def op(x: Double, y: Double) = x - y }
  case object mult extends Operand with CalcTerm with HigherPrecedence {override def op(x: Double, y: Double) = x * y }
  case object div extends Operand with CalcTerm with HigherPrecedence {override def op(x: Double, y: Double) = x / y }

  val map: Map[String, Operand] = Map( "+" -> plus,  "-" -> minus, "/" -> div,  "*" -> mult)
  def getOperand(s: String): Option[Operand] = map.get(s)
}


class Value(val value: Double) extends CalcTerm {
  override def toString = s"$value"
}
object Value{
  def fromString(s: List[Char]): Value ={
    new Value(s.mkString.toDouble)
  }

}

object BasicCalculator {
  val operands = Seq('+', '-', '*', '/')
  val digits = Seq('1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.')


  private def parse(parsed: List[CalcTerm], l: List[Char]): List[CalcTerm] = {
    l match {
      case Nil => parsed
      case h :: t if (digits.contains(h)) => {
        val numChunk = l takeWhile (digits.contains(_))
        parse(parsed:+ Value.fromString(numChunk), l drop numChunk.length)
      }
      case h :: t if (operands.contains(h)) => Operand.getOperand(h toString).fold(throw new RuntimeException(s"Valid operator not found in map"))(op => parse(parsed :+op , t))
      case _ => throw new IllegalArgumentException("Parsing error")
    }
  }


  @tailrec
  private def multi(processed: List[CalcTerm], list: List[CalcTerm]): List[CalcTerm] = {
    list match {
      case Nil => processed
      case (x: Value) :: (y: HigherPrecedence) :: (z: Value) :: tail => multi(processed, new Value(y.op(x.value, z.value)) :: tail)
      case x :: tail => multi(processed :+ x, tail)
      case _ => throw new IllegalArgumentException("Malformed expression.")
    }
  }

  @tailrec
  private def sum(acc: Double, l: List[CalcTerm]): Double = {
    l match {
      case Nil => acc
      case (x:Value)::Nil => acc + x.value
      case (x: Value) :: (y: Operand) :: (z: Value) :: tail => sum(y.op(x.value, z.value), tail)
      case (y: Operand) :: (z: Value) :: tail => sum(y.op(acc, z.value), tail)
      case _ => acc
    }
  }

  def calculate(string: Option[String]): Option[Double] = {
    string.filter(_.trim.nonEmpty) map { s =>
      val hasTrailingOperand: Boolean =  Option(s.last) exists (operands.contains(_))
      val startsWithOperand : Boolean = s.headOption exists (c => operands.contains(c) && c != '-')
      if(hasTrailingOperand ||startsWithOperand)  throw new IllegalArgumentException("Operators must be between values")
      sum(0D, multi(Nil, parse(Nil, s.toList)))
    }
  }
}

object Main {
  def main(args: Array[String]) ={
  while (true){
      val ln = Option(scala.io.StdIn.readLine())
       Try(BasicCalculator.calculate(ln)) match {
         case Success(d) => d map{ v => println (if (v % 1 == 0) v.toInt else v)}
         case Failure(NonFatal(e)) => println(e.getMessage)
       }
    }
  }
}

