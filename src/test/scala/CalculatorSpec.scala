package com.novus.calculator
import org.scalatest._

class calcSpec extends FlatSpec with Matchers {
  "calculator " should "return the value if 1" in {
      BasicCalculator.calculate(Some("1")) should equal(Some(1D))
    }

  "calculator " should "respect operation order" in {
    BasicCalculator.calculate(Some("1+4*5")) should equal(Some(21D))
  }


  "calculator " should "return 0 if expresion is empty" in {
    BasicCalculator.calculate(Some("")) should equal(None)
  }


  "calculator " should "rfail with wron characters" in {
    intercept[IllegalArgumentException] {
      BasicCalculator.calculate(Some("7+4%123"))
    }
  }

  "calculator " should "respect operation order;" in {
    BasicCalculator.calculate(Some("4/2+5*2")) should equal(Some(12D))
  }

  "calculator" should " fail when an operand is not recognized" in {
    intercept[IllegalArgumentException]{
      BasicCalculator.calculate(Some("4/2+55%2"))
    }
  }
}
